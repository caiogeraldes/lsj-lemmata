# LSJ Lemmata

O documento `lsj.csv` contém todos os lemmas e entradas abreviadas do LSJ baseado na edição disponível em [PerseusDL/Lexica](https://github.com/PerseusDL/lexica).
Os dados foram extraídos do banco de dados `hib_lemmas.sql` utilizando um query em SQL como anotado em `Query.sql`, gerando assim o arquivo em *comma separated values* `lemmata.csv`.
Em seguida, o script `convert.py` realiza as tarefas de separar os dados em `lemmata.csv` que pertencem ao LSJ (`lemma_lang_id = 2`) e convertê-los para UTF-8.
O resultado está em `lsj.csv` e pode ser manipulado com mais facilidade que documentos em `.sql` por meio de scripts ou plataformas do tipo Excel ou Calc, selecionando `,` como separador e `"` como *string delimiter*.


# LICENÇA

De acordo com a licença utilizada pela Tufts University e Perseus Digital Library, produtos formulados diretamente a partir de seus dados devem seguir a CC-BY-SA-4.0, e assim o faço aqui.
