"""
Author: Caio Geraldes
License: CC-BY-SA-4.0

Script to generate the full LSJ list of words in Unicode from
a .csv generated from Perseus data.

"""


import pandas as pd
import betacode.conv as bt

with open("lemmata.csv") as file:
    df = pd.read_csv(file)

df = df[df.lemma_lang_id == 2]

lemma_unicode = []

for i in df.lemma_text:
    lemma_unicode.append(bt.beta_to_uni(i))

df["lemma_unicode"] = lemma_unicode

df = df[["lemma_id", "lemma_text", "lemma_unicode", "lemma_short_def"]]

df.to_csv("lsj.csv", index=False)
df.to_excel("lsj.xlsx", index=False)

